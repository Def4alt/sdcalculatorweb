import React, { useContext } from "react";
import { withAuthorization, AuthUserContext } from "../../context/session";
import { User } from "firebase";

import "../../styles/component/component.scss";
import "../../styles/avatar/avatar.scss";
import "../../styles/button/button.scss";

const Account: React.FC = () => {
    const user = useContext(AuthUserContext) as User;

    const avatar = user.photoURL as string;

    return (
        <div className="component component_centered">
            <div className="component__element component__element_centered">
                <img
                    className="avatar avatar_big avatar_squared"
                    src={avatar}
                    alt="avatar"
                />
            </div>
        </div>
    );
};

export default withAuthorization(
    (authUser: firebase.User | null) => !!authUser
)(Account);
