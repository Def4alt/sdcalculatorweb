import Westgard from "./westgard";

let westgard: Westgard;

beforeEach(() => {
    westgard = new Westgard(10);
})

test("checks rule 13S", () => {
    westgard.setAverageArr([10, 50]);
    expect(westgard.check()).toBe("13S");
    westgard.setAverageArr([10, -50]);
    expect(westgard.check()).toBe("13S");
})

test("checks rule R4S", () => {
    westgard.setAverageArr([10, 50, -50]);
    expect(westgard.check()).toBe("R4S");
    westgard.setAverageArr([10, -50, 50]);
    expect(westgard.check()).toBe("R4S");
})

test("checks rule 22S", () => {
    westgard.setAverageArr([10, 31, 31]);
    expect(westgard.check()).toBe("22S");
    westgard.setAverageArr([10, -11, -11]);
    expect(westgard.check()).toBe("22S");
})

test("checks rule 41S", () => {
    westgard.setAverageArr([10, 21, 21, 21, 21]);
    expect(westgard.check()).toBe("41S");
    westgard.setAverageArr([10, -21, -21, -21, -21]);
    expect(westgard.check()).toBe("41S");
})

test("checks rule 8X", () => {
    westgard.setAverageArr([10, 11, 11, 11, 11, 11, 11, 11, 11]);
    expect(westgard.check()).toBe("8X");
    westgard.setAverageArr([10, -11, -11, -11, -11, -11, -11, -11, -11]);
    expect(westgard.check()).toBe("8X");
})

test("gets exceeds", () => {
    westgard.setAverageArr([10, 50]);
    expect(westgard.getExceeds(0, 3).plus).toBe(1);
    westgard.setAverageArr([10, 50, -50]);
    expect(westgard.getExceeds(1, 3).plus).toBe(1);
    expect(westgard.getExceeds(1, 3).minus).toBe(1);
    westgard.setAverageArr([10]);
    expect(westgard.getExceeds(1, 3).plus).toBe(0);
})

test("no array if perfect averages", () => {
    westgard.setAverageArr([10, 10, 10, 10, 10, 10, 10]);
    expect(westgard.check()).toBe(" ");
})