export default class Westgard {
	private sd: number;
	private averageArr: number[];

	private index: number = 1;

	constructor(sd: number, average: number[] = []) {
		this.sd = sd;
		this.averageArr = average;
	}

	public setSd = (value: number) => (this.sd = value);
	public setAverageArr = (values: number[]) => (this.averageArr = values);

	public check = (): string => {
		for (let i = 1; i < this.averageArr.length; i++) {
			const valuesLeft = this.averageArr.length - i;

			this.index = i;

			if (valuesLeft >= 8 && this.rule8X()) return "8X";

			if (valuesLeft >= 4 && this.rule41S()) return "41S";

			if (valuesLeft >= 2) {
				if (this.rule22S()) return "22S";
				else if (this.ruleR4S()) return "R4S";
			}

			if (this.rule13S()) return "13S";
		}

		return " ";
	};

	public rule8X = () => {
		const exceeds = this.getExceeds(8, 0);

		return exceeds.plus === 8 || exceeds.minus === 8;
	};

	public getExceeds = (indexRange: number, factor: number): ExceedsTuple => {
		const range = this.averageArr.filter(
			(_, i) => i >= this.index && i <= indexRange + this.index
		);

		const numOfPlusExceeds = range.filter((average) =>
			this.exceedsPlusSd(average, factor)
		).length;
		const numOfMinusExceeds = range.filter((average) =>
			this.exceedsMinusSd(average, factor)
		).length;

		return new ExceedsTuple(numOfPlusExceeds, numOfMinusExceeds);
	}

	public exceedsPlusSd = (value: number, factor: number) => {
		const globalAverage = this.averageArr[0];
		return value > globalAverage + this.sd * factor;
	};

	public exceedsMinusSd = (value: number, factor: number) => {
		const globalAverage = this.averageArr[0];
		return value < globalAverage - this.sd * factor;
	};

	public rule41S = () => {
		const exceeds = this.getExceeds(4, 1);

		return exceeds.plus === 4 || exceeds.minus === 4;
	};

	public rule22S = () => {
		const exceeds = this.getExceeds(1, 2);

		return exceeds.plus === 2 || exceeds.minus === 2;
	};

	public ruleR4S = () => {
		const exceeds = this.getExceeds(1, 2);

		return exceeds.plus === 1 && exceeds.minus === 1;
	};

	public rule13S = () => {
		const exceeds = this.getExceeds(0, 3);

		return exceeds.plus === 1 || exceeds.minus === 1;
	};
}

export class ExceedsTuple {
	public plus: number;
	public minus: number;

	constructor(plus: number, minus: number) {
		this.plus = plus;
		this.minus = minus;
	}
}