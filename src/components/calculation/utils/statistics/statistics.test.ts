import {
	getStatModels,
	getNonFailedModels,
	calculateStats,
	getAverageFor,
	getStandardDeviationOf,
} from "./statistics";

import { ReadModel, SampleType } from "../reader/reader";

test("gets statistics models", () => {
	const date = new Date().toUTCString();

	const readModels = [
		{
			Date: [date],
			FailedTests: ["failed_test"],
			SampleType: SampleType.Lvl1,
			TestResults: {
				test: 10,
				failed_test: 20,
			},
		} as ReadModel,
		{
			Date: [date],
			FailedTests: ["failed_test"],
			SampleType: SampleType.Lvl1,
			TestResults: {
				test: 20,
				failed_test: 20,
			},
		} as ReadModel,
		{
			Date: [date],
			FailedTests: ["failed_test"],
			SampleType: SampleType.Lvl1,
			TestResults: {
				test: 30,
				failed_test: 20,
			},
		} as ReadModel,
	];

	const models = getStatModels(readModels);

	expect(models.length).toBe(1);
	expect(models[0].Average[0]).toBe(20);
	expect(models[0].SampleType).toBe(SampleType.Lvl1);
	expect(models[0].TestName).toBe("test");
	expect(models[0].Date[0]).toBe(date);
	expect(models[0].Warnings.length).toBe(1);
});

test("gets non failed models", () => {
	const readModels = [
		{
			TestResults: { test: 20, failed: 20, partKnown: 50 },
			SampleType: SampleType.Lvl1,
			Date: [],
			FailedTests: ["failed", "max_failed"],
		} as ReadModel,
		{
			TestResults: { test: 10, failed: 30 },
			SampleType: SampleType.Lvl1,
			Date: [],
			FailedTests: ["max_failed"],
		} as ReadModel,
	];

	const nonFailed = getNonFailedModels(readModels, "test");
	const failed = getNonFailedModels(readModels, "failed");
	const maxFailed = getNonFailedModels(readModels, "max_failed");
	const partiallyKnown = getNonFailedModels(readModels, "partKnown");
	const unknown = getNonFailedModels(readModels, "unknown");

	expect(nonFailed.length).toBe(2);
	expect(failed.length).toBe(1);
	expect(maxFailed.length).toBe(0);
	expect(partiallyKnown.length).toBe(1);
	expect(unknown.length).toBe(0);
});

test("gets model", () => {
	const readModels = [
		{
			TestResults: { test: 20, failed: 20, partKnown: 50 },
			SampleType: SampleType.Lvl1,
			Date: [],
			FailedTests: ["failed", "max_failed"],
		} as ReadModel,
		{
			TestResults: { test: 10, failed: 30 },
			SampleType: SampleType.Lvl1,
			Date: [],
			FailedTests: ["max_failed"],
		} as ReadModel,
	];

	const model = calculateStats(readModels, "test", SampleType.Lvl1);

	expect(model.Average[0]).toBe(15);
	expect(model.SampleType).toBe(SampleType.Lvl1);
	expect(model.TestName).toBe("test");
	expect(model.SD).toBe(5);
});

test("gets average", () => {
	expect(getAverageFor([10, 20, 30])).toBe(20);
	expect(getAverageFor([])).toBe(0);
	expect(getAverageFor([0])).toBe(0);
});

test("gets standard deviation", () => {
	expect(getStandardDeviationOf([10, 20])).toBe(5);
	expect(getStandardDeviationOf([])).toBe(0);
});
