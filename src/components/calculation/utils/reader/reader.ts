import { read, Sheet, utils, CellObject } from "xlsx";
import moment from "moment";

export enum SampleType {
	Null = 0,
	Lvl1 = 1,
	Lvl2 = 2,
}

export interface Dictionary<T> {
	[x: string]: T;
}

export type ReadModel = {
	SampleType: SampleType;
	FailedTests: Array<string>;
	TestResults: Dictionary<number>;
	Date: Array<string>;
};

export const getReadModels = async (files: File[]) => {
	const types = /(\.xls|\.xlsx)$/i;

	const validFiles = files.filter((file) => file.name.match(types));

	const readModelsPerFile = validFiles.map((file) => readFile(file));
	const readModels = await flatenReadModels(readModelsPerFile);

	return readModels;
};

export const flatenReadModels = async (models: Promise<ReadModel[]>[]) => {
	let flattenedModels: ReadModel[] = [];
	for (const modelsPromise of models)
		flattenedModels = flattenedModels.concat(await modelsPromise);

	return flattenedModels;
};

export const readFile = async (file: File): Promise<ReadModel[]> => {
	return new Promise<ReadModel[]>((resolve) => {
		const reader = new FileReader();
		reader.readAsBinaryString(file);

		reader.onload = () => {
			const workbook = read(reader.result, {
				type: "binary",
			});

			const sheet = workbook.Sheets[workbook.SheetNames[0]];

			const models = readSheet(sheet);
			resolve(models);
		};
	});
};

export const readSheet = (sheet: Sheet): ReadModel[] => {
	const sheetRange = utils.decode_range(sheet["!ref"] || "");
	const valueRow = 4;
	const startRow = sheetRange.s.r + valueRow;
	const endRow = sheetRange.e.r;

	const builder = new ReadModelBuilder(sheet);

	const models = range(startRow, endRow + 1)
		.filter((row) => {
			builder.setRow(row);
			return builder.getSampleType() !== SampleType.Null;
		})
		.map((row) => {
			builder.setRow(row);
			return builder.getModel();
		});

	return models;
};

const range = (start: number, end: number) =>
	Array.from({ length: end - start }, (v, k) => k + start);

export class ReadModelBuilder {
	public sheet: Sheet;
	public row: number;

	constructor(sheet: Sheet = {}, row: number = NaN) {
		this.sheet = sheet;
		this.row = row;
	}

	public setRow = (row: number) => (this.row = row);
	public setSheet = (sheet: Sheet) => (this.sheet = sheet);

	public getModel = () => {
		const model: ReadModel = {
			Date: [this.getDate()],
			FailedTests: this.getFailedTests(),
			SampleType: this.getSampleType(),
			TestResults: this.getTestResults(),
		};

		return model;
	};

	public getDate = () => {
		const dateCheck = /\d{2}_\d{2}_\d{2}/i;
		const dateColumn = 0;

		const cellValue = this.tryGetValueFromCell(dateColumn).toString();

		const regexArr = dateCheck.exec(cellValue);

		if (regexArr) return moment(regexArr[0], "DD_MM_YY").toDate().toUTCString();
		else return new Date().toUTCString();
	};

	public getFailedTests = () => {
		const failedTestsColumn = 5;
		const failedTestsCell = this.tryGetValueFromCell(failedTestsColumn);

		if (failedTestsCell === "") return [];

		return String(failedTestsCell)
			.split(",")
			.map((t) => t.trim());
	};

	public tryGetValueFromCell = (col: number, row: number = this.row) => {
		try {
			return this.getValueFromCell(col, row);
		} catch (e) {
			console.log((e as Error).message);
			return "";
		}
	};

	public getValueFromCell = (
		col: number,
		row: number = this.row
	): string | number => {
		const cell = utils.encode_cell({
			r: row,
			c: col,
		});
		const value = this.sheet[cell];

		this.validateCellValue(value, cell);

		return value.v;
	};

	private validateCellValue = (value: CellObject, cell: string) => {
		if (!value || value.v === undefined)
			throw new XlsxFailedToGetCellValue(`Failed to get value from ${cell}`);
	};

	public getSampleType = () => {
		const sampleTypeColumn = 3;
		const cellValue = this.tryGetValueFromCell(sampleTypeColumn);

		if (cellValue.toString().toUpperCase().trim() === "QC LV I")
			return SampleType.Lvl1;
		if (cellValue.toString().toUpperCase().trim() === "QC LV II")
			return SampleType.Lvl2;

		return SampleType.Null;
	};

	public getTestResults = () => {
		const sheetRange = utils.decode_range(this.sheet["!ref"] || "");

		const testResultsColumn = 6;
		const testTitleRow = 2;
		const startColumn = sheetRange.s.c + testResultsColumn;
		const encColumn = sheetRange.e.c;

		let testResults: Dictionary<number> = {};

		range(startColumn, encColumn + 1).forEach((col) => {
			const testTitle = this.getTestTitle(col, testTitleRow);
			if (testTitle === "") return;

			const testValue = this.tryGetTestValue(col);
			if (isNaN(testValue)) return;

			testResults[testTitle] = testValue;
		});

		return testResults;
	};

	public getTestTitle = (col: number, row: number) => {
		const cellValue = this.tryGetValueFromCell(col, row);

		if (cellValue === "" || cellValue.toString().includes("/")) return "";

		return cellValue.toString().trim();
	};

	public tryGetTestValue = (col: number) => {
		try {
			return this.getTestValue(col);
		} catch (e) {
			console.log((e as Error).message);
			return NaN;
		}
	};

	public getTestValue = (col: number) => {
		const cellValue = this.tryGetValueFromCell(col);

		this.validateTestValue(cellValue);

		return cellValue as number;
	};

	private validateTestValue = (cellValue: string | number) => {
		if (cellValue === "" || isNaN(cellValue as number))
			throw new FailedToParse(
				`Failed to parse "${cellValue.toString()}" to number`
			);
	};
}

export class XlsxFailedToGetCellValue extends Error {}

export class FailedToParse extends Error {}
