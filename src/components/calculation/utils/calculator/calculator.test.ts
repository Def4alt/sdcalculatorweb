import { calculate, appendModels, getWarning } from "./calculator"
import fs from "fs"
import { SampleType } from "../reader/reader";
import { StatModel } from "../statistics/statistics";

test("calculates", async () => {
    const file = await new Promise<File>(resolve => nodeGetFile(resolve));

    const models = await calculate([file], [], true);

    expect(models.length).toBe(55);

    expect(models[0].SampleType).toBe(SampleType.Lvl1);
})

const nodeGetFile = (
	resolve: (value?: File | PromiseLike<File> | undefined) => void
) => {
	fs.readFile(
		"src/components/calculation/utils/reader/test/test_file.xlsx",
		(err: NodeJS.ErrnoException | null, data: Buffer) => {
			if (err) throw err;
			const ab = new ArrayBuffer(data.length);
			let view = new Uint8Array(ab);
			for (let i = 0; i < data.length; ++i) {
				view[i] = data[i];
			}
			const file = new File([ab], "test_file.xlsx");
			resolve(file);
		}
	);
};

test("appends new models", () => {
    const newModels = [
        {
            SampleType: SampleType.Lvl1,
            Date: [new Date().toUTCString()],
            Average: [10],
            TestName: "test",
            SD: 5,
            Warnings: [" "]
        } as StatModel
    ];

    const oldModels = [
        {
            SampleType: SampleType.Lvl1,
            Date: [new Date().toUTCString()],
            Average: [20],
            TestName: "test",
            SD: 5,
            Warnings: [" "]
        } as StatModel
    ];

    const appendedModels = appendModels(oldModels, newModels);
    
    expect(appendedModels.length).toBe(1);
    expect(appendedModels[0].Average.length).toBe(2);
})

test("gets warning", () => {
    let model = {
        Average: [10, 50],
        SD: 10,
        Warnings: ["13S"]
    } as StatModel;

    expect(getWarning(model)).toBe(" ");

    model.Average = [10, 50, -50];

    expect(getWarning(model)).toBe("R4S");

    model.Average = [10];
    
    expect(getWarning(model)).toBe(" ");
})