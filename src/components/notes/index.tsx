import React, {
    Reducer,
    useContext,
    useEffect,
    useReducer,
    useRef,
} from "react";
import { FirebaseContext } from "../../context/firebase";
import { AuthUserContext } from "../../context/session";
import { GoNote } from "react-icons/go";
import { LocalizationContext } from "../../context/localization";

import "../../styles/notes/notes.scss";
import "../../styles/component/component.scss";
import "../../styles/button/button.scss";
import "../../styles/header/header.scss";

interface NotesProps {
    lot: string;
}

interface NotesState {
    methodName?: string;
    operatorName?: string;
    foundingDate?: string;
    materialNameAndManufacturer?: string;
    materialExpDate?: string;
    materialLvl1?: string;
    materialLvl2?: string;
    machineName?: string;
}

interface Action {
    type?: string;
    payload: NotesState;
}

const reducer: Reducer<NotesState, Action> = (state, action) => {
    switch (action.type) {
        default:
            return Object.assign({}, state, action.payload);
    }
};

const Notes: React.FC<NotesProps> = (props) => {
    const [notes, dispatch] = useReducer<
        Reducer<NotesState, Action>,
        NotesState
    >(reducer, {}, () => {
        return {} as NotesState;
    });

    const notesRef = useRef<HTMLFormElement | null>(null);
    const floatingNotesRef = useRef<HTMLFormElement | null>(null);

    const firebase = useContext(FirebaseContext);
    const user = useContext(AuthUserContext);
    const localization = useContext(LocalizationContext).localization;

    useEffect(() => {
        if (!user || !firebase || !props.lot) return;

        const lotDoc = firebase.getLot(user.uid, props.lot);

        lotDoc.then((snapshot) => {
            const notes = snapshot.data()?.notes as NotesState;

            if (notes) dispatch({ payload: notes });
        });
    }, [firebase, props.lot, user]);

    const toggleMenu = (
        ref: React.RefObject<HTMLElement>,
        className: string
    ) => {
        const menu = ref.current;

        if (!menu) return;

        if (!menu.classList.contains(className)) menu.classList.add(className);
        else menu.classList.remove(className);
    };

    const onSubmit = async (event: React.FormEvent) => {
        event.preventDefault();

        if (!user || !firebase) return;

        const lotDoc = await firebase.getLot(user.uid, props.lot);

        firebase.setLot(user.uid, props.lot, {
            models: lotDoc.data()?.models,
            notes,
        });
    };

    const onInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        dispatch({
            payload: { [e.currentTarget.name]: e.currentTarget.value },
        });
    };

    return (
        <div className="notes">
            <button
                className="notes__toggle button_icon"
                onClick={() => {
                    toggleMenu(notesRef, "notes__form_expanded");
                    toggleMenu(floatingNotesRef, "notes__form_expanded");
                }}
            >
                <GoNote />
            </button>
            <form
                className="notes__form notes__form_elevated"
                ref={floatingNotesRef}
                onSubmit={onSubmit}
            >
                <p className="notes__title">{localization.methodName}</p>
                <input
                    className="notes__input"
                    defaultValue={notes.methodName}
                    name="methodName"
                    type="text"
                    onChange={onInput}
                />

                <p className="notes__title">{localization.operatorName}</p>
                <input
                    className="notes__input"
                    defaultValue={notes.operatorName}
                    name="operatorName"
                    type="text"
                    onChange={onInput}
                />

                <p className="notes__title">{localization.machineName}</p>
                <input
                    className="notes__input"
                    defaultValue={notes.machineName}
                    name="machineName"
                    type="text"
                    onChange={onInput}
                />

                <p className="notes__title">{localization.foundingDate}</p>
                <input
                    className="notes__input"
                    defaultValue={notes.foundingDate}
                    name="foundingDate"
                    type="date"
                    onChange={onInput}
                />
                <br />

                <p className="notes__title">{localization.controlMaterial}</p>
                <div className="notes__level">
                    <p className="notes__label">
                        {localization.materialName} /{" "}
                        {localization.materialManufacturer}
                    </p>
                    <input
                        className="notes__input"
                        defaultValue={notes.materialNameAndManufacturer}
                        name="materialNameAndManufacturer"
                        type="text"
                        onChange={onInput}
                    />
                    <p className="notes__label">
                        {localization.materialExpDate}
                    </p>
                    <input
                        className="notes__input"
                        defaultValue={notes.materialExpDate}
                        name="materialExpDate"
                        type="date"
                        onChange={onInput}
                    />
                    <p className="notes__label">{localization.materialLvl1}</p>
                    <input
                        className="notes__input"
                        defaultValue={notes.materialLvl1}
                        name="materialLvl1"
                        type="text"
                        onChange={onInput}
                    />
                    <p className="notes__label">{localization.materialLvl2}</p>
                    <input
                        className="notes__input"
                        defaultValue={notes.materialLvl2}
                        name="materialLvl2"
                        type="text"
                        onChange={onInput}
                    />
                </div>

                <br />
                <br />
                <button className="button notes__submit">
                    {localization.submit}
                </button>
            </form>
            <form
                className="notes__form"
                ref={notesRef}
                onSubmit={onSubmit}
            ></form>
        </div>
    );
};

export default Notes;
