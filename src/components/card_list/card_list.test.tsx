import React from "react"
import { cleanup, RenderResult, render } from "@testing-library/react"
import CardsList from ".";
import { StatModel, SampleType } from "../../types";

afterEach(cleanup)

let component: RenderResult;

beforeEach(() => {
    component = render(<CardsList models={[{
        TestName: "test",
        Average: [10],
        SD: 10,
        Warnings: [" "],
        Date: [new Date().toUTCString()],
        SampleType: SampleType.Lvl1} as StatModel]}></CardsList>);
})

test("expect number of nodes to be 2 (header + 1 card)", () => {
    expect(component.container.firstChild?.childNodes).toHaveLength(2);
})

test("shows sd / cv by default", () => {
    expect(component.getByText("Hide SD / CV")).toBeInTheDocument();
})