const SampleType = Object.freeze({
    "Null": 0,
    "Lvl1": 1,
    "Lvl2": 2
})

export default SampleType;