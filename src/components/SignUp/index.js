import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import { compose } from "recompose";

import { withFirebase } from "../Firebase";
import * as ROUTES from "../../constants/routes";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { useTheme } from "../Theme";

const SignUpPage = props => (
	<div style={{ color: props.theme.theme.color, paddingLeft: 10, marginRight: "60vw" }}>
		<h1>Sign Up</h1>
		<SignUpForm />
	</div>
);

const INITIAL_STATE = {
	username: "",
	email: "",
	passwordOne: "",
	passwordTwo: "",
	error: null
};

class SignUpFormBase extends Component {
	constructor(props) {
		super(props);

		this.state = { ...INITIAL_STATE };
	}
	onSubmit = event => {
		const { username, email, passwordOne } = this.state;
		this.props.firebase
			.doCreateUserWithEmailAndPassword(email, passwordOne)
			.then(authUser => {
				// Create a user in your Firebase realtime database
				return this.props.firebase.user(authUser.user.uid).set({
					username,
					email
				});
			})
			.then(authUser => {
				this.setState({ ...INITIAL_STATE });
				this.props.history.push(ROUTES.LANDING);
			})
			.catch(error => {
				this.setState({ error });
			});
		event.preventDefault();
	};

	onChange = event => {
		this.setState({ [event.target.name]: event.target.value });
	};

	render() {
		const { username, email, passwordOne, passwordTwo, error } = this.state;
		const isInvalid =
			passwordOne !== passwordTwo ||
			passwordOne === "" ||
			email === "" ||
			username === "";
		return (
			<Form onSubmit={this.onSubmit}>
				<Form.Group>
					<Form.Label>Username</Form.Label>

					<Form.Control
						name="username"
						value={username}
						onChange={this.onChange}
						type="text"
						placeholder="Full Name"
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Email address</Form.Label>

					<Form.Control
						name="email"
						value={email}
						onChange={this.onChange}
						type="text"
						placeholder="email@example.com"
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control
						name="passwordOne"
						value={passwordOne}
						onChange={this.onChange}
						type="password"
						placeholder="E.g. pasdgf@#!1254"
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Repeat Password</Form.Label>
					<Form.Control
						name="passwordTwo"
						value={passwordTwo}
						onChange={this.onChange}
						type="password"
					/>
				</Form.Group>
				<Button disabled={isInvalid} type="submit">
					Sign Up
				</Button>
				<Form.Text className="text-danger">
					{error && <p>{error.message}</p>}
				</Form.Text>
			</Form>
		);
	}
}

const SignUpLink = () => (
	<p>
		Don't have an account? <Link to={ROUTES.SIGN_UP}>Sign Up</Link>
	</p>
);

const SignUpForm = compose(
	withRouter,
	withFirebase
)(SignUpFormBase);

export default useTheme(SignUpPage);
export { SignUpForm, SignUpLink };
