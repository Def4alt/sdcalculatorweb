import React from "react";
import { cleanup, render } from "@testing-library/react";
import About from ".";

afterEach(cleanup);

test("about page renders", () => {
    const { getByText } = render(<About></About>);

    expect(getByText("Made for ChemoView™")).toBeInTheDocument();
});
