import Lot from "./index";
import React from "react";
import {
	cleanup,
	RenderResult,
	render,
	fireEvent,
} from "@testing-library/react";

afterEach(cleanup);

let component: RenderResult;
let addLotButton: Element;

beforeEach(() => {
	component = render(
		<Lot
			callback={(lot: string) => {
				return;
			}}
		></Lot>
	);
	addLotButton = component.container.querySelector(
		".edit__add"
	) as Element;
});

test("No lot input by default", () => {
	expect(addLotButton).toBeInTheDocument();
});

test("lot input accepts only numbers less than 6 digits", () => {
	fireEvent.click(addLotButton);

	const input = component.container.querySelector(
		"#lot-input"
	) as HTMLInputElement;

	fireEvent.change(input, { target: { value: "1423" } });

	expect(input.value).toBe("1423");

	fireEvent.change(input, { target: { value: "text123" } });

	expect(input.value).toBe("1423");

	fireEvent.change(input, { target: { value: "1244353" } });

	expect(input.value).toBe("1423");
});

test("lot list is empty by default", () => {
	const lotList = component.container.querySelectorAll(".edit__cell");

	expect(lotList).toHaveLength(0);
});

test("add lot after input and confirmation", () => {
	addLot("1234");

	const lotList = component.container.querySelectorAll(".edit__cell");

	expect(lotList).toHaveLength(1);
});

const addLot = (lot: string) => {
	const addLotButton = component.container.querySelector(
		".edit__add"
	) as Element;

	fireEvent.click(addLotButton);

	const input = component.container.querySelector(
		"#lot-input"
	) as HTMLInputElement;

	fireEvent.change(input, { target: { value: lot } });

	const confirmationButton = component.container.querySelector(
		"#confirm-add-lot-button"
	) as Element;

	fireEvent.click(confirmationButton);
};

test("would not add empty lot", () => {
	addLot("");

	const lotList = component.container.querySelectorAll(".edit__cell");

	expect(lotList).toHaveLength(0);
});

test("removes lot", () => {
	addLot("123");

	const removeButton = component.container
		.querySelectorAll(".edit__remove")
		.item(0);

	fireEvent.click(removeButton);

	const lotList = component.container.querySelectorAll(".edit__cell");

	expect(lotList).toHaveLength(0);
});

test("removes correct lot", () => {
	addLot("123");
	addLot("253");
	addLot("354");

	const removeButton0 = component.container
		.querySelectorAll(".edit__remove")
		.item(0);
	fireEvent.click(removeButton0);

	const removeButton1 = component.container
		.querySelectorAll(".edit__remove")
		.item(0);
	fireEvent.click(removeButton1);

	const lotList = component.container.querySelectorAll(".edit__cell");

	expect(lotList).toHaveLength(1);
	expect(lotList[0].firstChild?.textContent).toBe("354");
});
