import app from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";
import firebase from "firebase";

const firebaseConfig = {
    apiKey: process.env.FIREBASE_API_KEY,
    authDomain: process.env.FIREBASE_AUTH_DOMAIN,
    projectId: process.env.FIREBASE_PROJECT_ID,
    storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
    appId: process.env.FIREBASE_APP_ID,
    measurementId: process.env.FIREBASE_MEASUREMENT_ID,
};
class Firebase {
    auth: firebase.auth.Auth;
    db: firebase.firestore.Firestore;
    fbProvider: firebase.auth.AuthProvider;
    glProvider: firebase.auth.AuthProvider;
    storage: firebase.storage.Reference;

    constructor() {
        if (!app.apps.length) app.initializeApp(firebaseConfig);

        this.auth = app.auth();

        this.fbProvider = new app.auth.FacebookAuthProvider();
        this.glProvider = new app.auth.GoogleAuthProvider();
        this.auth.useDeviceLanguage();

        this.db = app.firestore();
        this.storage = app.storage().ref();
    }

    doCreateUserWithEmailAndPassword = (email: string, password: string) =>
        this.auth
            .createUserWithEmailAndPassword(email, password)
            .then(async (user) => {
                const defaultAvatarsCount = 25;
                const index = Math.floor(Math.random() * defaultAvatarsCount);

                const url = await this.storage
                    .child(`defaultAvatars/${index}.png`)
                    .getDownloadURL();

                user.user?.updateProfile({
                    photoURL: url,
                });

                return user;
            });

    doSignInWithEmailAndPassword = (email: string, password: string) =>
        this.auth.signInWithEmailAndPassword(email, password);

    doSignInWithGoogle = () => this.auth.signInWithPopup(this.glProvider);
    doSignInWithFacebook = () => this.auth.signInWithPopup(this.fbProvider);

    doSignOut = () => this.auth.signOut();

    doPasswordReset = (email: string) =>
        this.auth.sendPasswordResetEmail(email);

    doPasswordUpdate = (password: string) =>
        this.auth.currentUser && this.auth.currentUser.updatePassword(password);
        
    backup = (uid: string) => this.db.collection("backups").doc(uid);
    lots = (uid: string) => this.backup(uid).collection("lots");

    setLot = (
        uid: string,
        lot: string,
        data: firebase.firestore.DocumentData
    ) => this.lots(uid).doc(lot).set(data);

    getLot = (uid: string, lot: string) => this.lots(uid).doc(lot).get();
}

export default Firebase;
