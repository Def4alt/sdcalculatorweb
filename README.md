# SDCalculator on Web

[Website](https://sdcalculatorweb.web.app/)

## About 

Every patient needs in the accuracy of his blood tests for making a correct diagnosis. 
But every machine that collects data about blood errors. An error can be systematic or 
random. For calculating these errors my program was created. To identify the errors you 
can use control maps. My program calculates average and index of standard deviation for 
a series of control samples and builds a chart based on that data (the chart is a 
control map itself). Each following analysis calculates an average for a tuple of 
samples and adds a point to the graph. This procedure continues while the analysis of 
the patient proceeds. For evaluation of errors of calculation use Westgard Rules.

